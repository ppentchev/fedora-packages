Name:		spiped
Version:	1.6.2
Release:	2%{?dist}
Summary:	Create secure pipes between socket addresses

License:	BSD-2-Clause
URL:		https://www.tarsnap.com/spiped.html
Source0:	https://www.tarsnap.com/spiped/spiped-%{version}.tgz

# Use install(1) to install programs and manual pages; honor DESTDIR.
# Upstream indicated via e-mail that install(1) is not standardized by
# POSIX and it would be difficult to make the Makefile configurable.
Patch0:		install.patch

BuildRequires:	gcc
BuildRequires:	make
BuildRequires:	openssl-devel
BuildRequires:	procps-ng

%description

spiped (pronounced "ess-pipe-dee") is a utility for creating symmetrically
encrypted and authenticated pipes between socket addresses, so that one may
connect to one address (e.g., a UNIX socket on localhost) and transparently
have a connection established to another address (e.g., a UNIX socket on a
different system).  This is similar to 'ssh -L' functionality, but does not
use SSH and requires a pre-shared symmetric key.

spipe (pronounced "ess-pipe") is a utility which acts as an spiped protocol
client (i.e., connects to an spiped daemon), taking input from the standard
input and writing data read back to the standard output.

%prep
%autosetup -p1


%build
%{make_build}


%check
make test


%install
%{make_install} BINDIR=%{_bindir} MAN1DIR=%{_mandir}/man1 


%files
%license COPYRIGHT
%{_bindir}/spipe
%{_bindir}/spiped
%{_mandir}/man1/spipe.1*
%{_mandir}/man1/spiped.1*

%changelog
* Wed Mar 20 2024 Peter Pentchev <roam@ringlet.net> - 1.6.2-2
- Apply several suggestions from Artur Frenszek-Iwicki:
  - drop the "Group" tag
  - use %{version} in the Source0 line
  - add a comment about the install patch to the specfile too, not only in
    the patch file itself
  - do not use the internal %{__make} macro in %check
  - use wildcards for the manual pages

* Sat Feb 24 2024 Peter Pentchev <roam@ringlet.net> - 1.6.2-1
- Initial packaging.
